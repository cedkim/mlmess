import numpy as np
import os
import PIL
import PIL.Image
import tensorflow as tf
from archive import extract
import warnings
import pathlib
import ipywidgets as widgets
from IPython.display import display, FileLink, FileLinks
from IPython.display import Image as displayImage
def mainstuff():
    filename = ""
    class_names = []
    model = None
    num_classes = 0
    t1 = widgets.Output()
    t2 = widgets.Output()
    t3 = widgets.Output()
    t4 = widgets.Output()
    t = widgets.Tab()
    t.children = [t1, t2, t3, t4]
    t.set_title(0, "1. Upload images")
    t.set_title(1, "2. Train your model")
    t.set_title(2, "3. Test your model")
    t.set_title(3, "4. Download your model")
    display(t)
    def resetImages(*args):
        if (os.path.isdir("photos")):
            os.rmdir("photos")
        uploadImages()
    def clearAndResetImages(*args):
        t1.clear_output()
        resetImages()
    def uploadImages(*args):
        print("Welcome to Image Sorter Builder!\nThis is a tool for building machine learning models for TensorFlow that\nsort images into categories.\nFirst, you'll need to upload some pictures to 'teach' your model\nwhat it'll be sorting.\nThe pictures need to be in a ZIP, TAR.GZ, or TGZ file containing folders for each category\nyou want to create, with the respective images inside.\nIt's optimal if you have a LOT of images (a few thousand is best).\n")
        u = widgets.FileUpload(accept=".zip,.tar.gz,.tgz")
        display(u)
        while u.value == {}:
            pass
        [foof] = u.value
        f = [foof["name"]]
        with open(f[0], "wb") as fp:
            fp.write(u.value["content"])
        if(len(f) > 0) and ((".zip" in f[0]) == False and (".tar.gz" in f[0]) == False) and (".tgz" in f[0]) == False:
            t1.clear_output()
            print("(Error: A file other than a .zip/.tar.gz/.tgz was uploaded. Try again.)\n")
            resetImages()
        elif(len(f) > 0):
            t1.clear_output()
            filename = f[0]
            extract(filename, "photos")
            class_names = os.walk("photos")
            print("Your images were successfully uploaded.\nHere's a list of categories:\n" + ", ".join([x[0] for x in class_names]) + "\n\nIf this doesn't look right, please click\nthe button below.")
            resetBtn = widgets.Button(description="Reset and try again")
            resetBtn.on_click(clearAndResetImages)
            display(resetBtn)
        else:
            t1.clear_output()
            print("(Error: No images were uploaded. Try again.)\n")
            resetImages()
    def trainModel(*args):
        t2.clear_output()
        batch_size = 32
        img_height = 180
        img_width = 180
        print("Starting preprocessing...")
        train_ds = tf.keras.preprocessing.image_dataset_from_directory(
            "photos",
            validation_split=0.2,
            subset="training",
            seed=123,
            image_size=(img_height, img_width),
            batch_size=batch_size
        )
        val_ds = tf.keras.preprocessing.image_dataset_from_directory(
            "photos",
            validation_split=0.2,
            subset="validation",
            seed=123,
            image_size=(img_height, img_width),
            batch_size=batch_size
        )
        print("Caching images...")
        AUTOTUNE = tf.data.experimental.AUTOTUNE
        train_ds = train_ds.cache().shuffle(1000).prefetch(buffer_size=AUTOTUNE)
        val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)
        print("Finished preprocessing.\nSetting up model...")
        num_classes = len(class_names)
        model = tf.keras.models.Sequential([
            tf.keras.layers.experimental.preprocessing.RandomFlip("horizontal", input_shape=(img_height, img_width, 3)),
            tf.keras.layers.experimental.preprocessing.RandomRotation(0.1),
            tf.keras.layers.experimental.preprocessing.RandomZoom(0.1),
            tf.keras.layers.experimental.preprocessing.Rescaling(1./255, input_shape=(img_height, img_width, 3)),
            tf.keras.layers.Conv2D(16, 3, padding='same', activation='relu'),
            tf.keras.layers.MaxPooling2D(),
            tf.keras.layers.Conv2D(32, 3, padding='same', activation='relu'),
            tf.keras.layers.MaxPooling2D(),
            tf.keras.layers.Conv2D(64, 3, padding='same', activation='relu'),
            tf.keras.layers.MaxPooling2D(),
            tf.keras.layers.Dropout(0.2),
            tf.keras.layers.Flatten(),
            tf.keras.layers.Dense(128, activation='relu'),
            tf.keras.layers.Dense(num_classes)
        ])
        model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
        max_epochs = 100
        early_stopping = tf.keras.callbacks.EarlyStopping(patience=2)
        model.fit(train_ds,
                  validation_data=val_ds,
                  epochs=max_epochs,
                  callbacks=[early_stopping],
                  verbose=2)
    def predictionScreen(*args):
        t3.clear_output()
        print("You can test your model here.\nUpload an image that wasn't in the dataset,\nand see if it can identify the image.")
        pu = widgets.FileUpload(accept="image/*")
        display(pu)
        while pu.value == {}:
            pass
        pf = [pu.value["name"]]
        with open(pf[0], "wb") as pfp:
            pfp.write(u.value["content"])
        if (len(pf) > 0):
            if ((".jpg" in pf[0]) == False) and ((".jpeg" in pf[0]) == False) and ((".png" in pf[0]) == False):
                predictionScreen()
            else:
                img = tf.keras.preprocessing.image.load_img(
                    pf[0], target_size=(img_height, img_width)
                )
                img_array = tf.keras.preprocessing.image.img_to_array(img)
                img_array = tf.expand_dims(img_array, 0)
                predictions = model.predict(img_array)
                score = tf.nn.softmax(predictions[0])
                displayImage(pf[0])
                print("\nThis image looks like a {}.\n".format(class_names[np.argmax(score)]))
                predictBtn = widgets.Button(description="Try another image")
                predictBtn.on_click(predictionScreen)
                display(predictBtn)
        else:
            predictionScreen()
    with t2:
        print("You haven't uploaded any images for your model to use during training.")
    with t3:
        print("You haven't trained your model, so you can't test it yet.")
    with t4:
        print("You haven't trained your model, so you can't export it yet.")
    with t1:
        uploadImages()
    with t2:
        t2.clear_output()
        print("Before you can use it, your machine learning model has to be trained.\nClick the button below to start training your model.\n")
        trainBtn = widgets.Button(description="Start training model")
        trainBtn.on_click(trainModel)
        display(trainBtn)
    with t4:
        t4.clear_output()
        model.save_weights('./imagesortermodel')
        print("You can export your model to a .ckpt file that can be used in Tensorflow.\nClick the link below to do so.\n")
        FileLink('imagesortermodel.ckpt')
    with t3:
        predictionScreen()
def load_ipython_extension(*args):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        mainstuff()